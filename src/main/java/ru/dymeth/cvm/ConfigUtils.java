package ru.dymeth.cvm;

import net.md_5.bungee.api.plugin.Plugin;
import net.md_5.bungee.config.Configuration;
import net.md_5.bungee.config.ConfigurationProvider;
import net.md_5.bungee.config.YamlConfiguration;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;

public class ConfigUtils {
    public static Configuration loadConfig(Plugin plugin, @SuppressWarnings("SameParameterValue") String fileName, boolean fromJar) {
        try {
            if (fromJar) {
                return ConfigurationProvider
                        .getProvider(YamlConfiguration.class)
                        .load(plugin.getResourceAsStream(fileName));
            }
            return ConfigurationProvider
                    .getProvider(YamlConfiguration.class)
                    .load(getConfigFile(plugin, fileName));
        } catch (Exception e) {
            throw new RuntimeException("Unable to load " + fileName, e);
        }
    }

    public static void saveConfig(Plugin plugin, @SuppressWarnings("SameParameterValue") String fileName, Configuration configuration) {
        try {
            ConfigurationProvider
                    .getProvider(YamlConfiguration.class)
                    .save(configuration, getConfigFile(plugin, fileName));
        } catch (Exception e) {
            throw new RuntimeException("Unable to save " + fileName, e);
        }
    }

    private static File getConfigFile(Plugin plugin, String fileName) throws IOException {
        if (!plugin.getDataFolder().exists() && !plugin.getDataFolder().mkdir()) {
            throw new RuntimeException("Unable to create plugin directory");
        }
        File result = new File(plugin.getDataFolder(), fileName);
        if (!result.exists()) {
            Files.copy(plugin.getResourceAsStream(fileName), result.toPath(), StandardCopyOption.REPLACE_EXISTING);
        }
        return result;
    }
}
