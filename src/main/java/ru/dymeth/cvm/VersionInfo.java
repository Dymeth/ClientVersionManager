package ru.dymeth.cvm;

public class VersionInfo {
    private final int protocolNumber;
    private final String displayName;

    public VersionInfo(int protocolNumber, String displayName) {
        this.protocolNumber = protocolNumber;
        this.displayName = displayName;
    }

    public int getProtocolNumber() {
        return this.protocolNumber;
    }

    public String getDisplayName() {
        return this.displayName;
    }
}
