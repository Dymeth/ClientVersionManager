package ru.dymeth.cvm;

import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.event.ServerConnectEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.api.plugin.Plugin;
import net.md_5.bungee.config.Configuration;
import net.md_5.bungee.event.EventHandler;

import java.util.*;
import java.util.stream.Collectors;

public class ClientVersionManager extends Plugin implements Listener {
    private final Map<String, VersionInfo> minServerVersions = new HashMap<>();
    private String denyMessage;

    @Override
    public void onEnable() {
        this.getProxy().getPluginManager().registerListener(this, this);
        Configuration baseConfig = ConfigUtils.loadConfig(this, "config.yml", false);
        Configuration baseConfigDefault = ConfigUtils.loadConfig(this, "config.yml", true);

        Map<String, VersionInfo> protocols = this.parseProtocols(baseConfig.getSection("Protocols"));
        Map<String, VersionInfo> protocolsDefault = this.parseProtocols(baseConfigDefault.getSection("Protocols"));
        List<VersionInfo> addedVersions = new ArrayList<>();
        for (VersionInfo version : protocolsDefault.values()) {
            if (protocols.containsKey(version.getDisplayName())) continue;
            addedVersions.add(version);
        }
        if (!addedVersions.isEmpty()) {
            Configuration protocolsConfigOld = baseConfig.getSection("Protocols");
            Configuration protocolsConfigNew = new Configuration();
            for (VersionInfo version : addedVersions) {
                protocols.put(version.getDisplayName(), version);
                protocolsConfigNew.set(version.getDisplayName().replace(".", "_"), version.getProtocolNumber());
            }
            for (String key : protocolsConfigOld.getKeys()) {
                protocolsConfigNew.set(key, protocolsConfigOld.get(key));
            }
            baseConfig.set("Protocols", null);
            baseConfig.set("Protocols-Info", "Actual protocol versions list: https://wiki.vg/Protocol_version_numbers ");
            baseConfig.set("Protocols", protocolsConfigNew);
            ConfigUtils.saveConfig(this, "config.yml", baseConfig);
            this.getLogger().info("Registered protocol versions for new Minecraft version(s): "
                    + addedVersions.stream().map(VersionInfo::getDisplayName).collect(Collectors.joining("; ")));
        }

        Configuration clientVersion = baseConfig.getSection("MinClientVersions");
        for (String serverName : clientVersion.getKeys()) {
            String versionName = clientVersion.getString(serverName);
            VersionInfo ver = protocols.get(versionName);
            if (ver == null) {
                this.getLogger().severe("Unknown protocol number of version " + versionName
                        + " (server " + serverName + ")."
                        + " Please fill \"Protocols\" section in config."
                        + " Available versions: https://wiki.vg/Protocol_version_numbers");
                continue;
            }
            if (this.getProxy().getServerInfo(serverName) == null) {
                this.getLogger().warning("Server " + serverName + " not found");
            }
            this.minServerVersions.put(serverName, ver);
        }

        this.denyMessage = ChatColor.translateAlternateColorCodes('&', baseConfig.getString("DenyMessage"));
    }

    private Map<String, VersionInfo> parseProtocols(Configuration protocolsConfig) {
        Map<String, VersionInfo> result = new LinkedHashMap<>();
        for (String versionName : protocolsConfig.getKeys()) {
            int number = protocolsConfig.getInt(versionName);
            versionName = versionName.replace("_", "."); // Dot is sections splitter in YAML
            result.put(versionName, new VersionInfo(number, versionName));
        }
        return result;
    }

    @EventHandler
    public void on(ServerConnectEvent event) {
        VersionInfo ver = this.minServerVersions.get(event.getTarget().getName());
        if (ver == null) return;
        if (event.getPlayer().getPendingConnection().getVersion() >= ver.getProtocolNumber()) return;
        BaseComponent[] failMsg = TextComponent.fromLegacyText(this.denyMessage
                .replace("%version%", ver.getDisplayName())
                .replace("%server%", event.getTarget().getName())
        );
        if (failMsg == null) return;
        event.setCancelled(true);
        if (event.getReason() == ServerConnectEvent.Reason.JOIN_PROXY)
            event.getPlayer().disconnect(failMsg);
        else
            event.getPlayer().sendMessage(failMsg);
    }
}
